package org.semeniv.alerts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class Alerts {

    WebDriver driver;

    @BeforeClass
    public void beforeClass(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void afterClass(){
        if (driver != null){
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod(){
        driver.get("http://localhost:7080/javascript_alerts");
    }

    @Test
    public void alertTest1(){

        driver.findElement(By.xpath("//button[text()='Click for JS Confirm']")).click();

        Alert alert = driver.switchTo().alert();

        alert.accept();

        String expectedText = driver.findElement(By.id("result")).getText();

        Assert.assertEquals(expectedText, "You clicked: Ok");
    }

    @Test
    public void alertTest2() {

        driver.findElement(By.xpath("//button[text()='Click for JS Confirm']")).click();

        Alert alert = driver.switchTo().alert();

        alert.dismiss();

        String expectedText = driver.findElement(By.id("result")).getText();

        Assert.assertEquals(expectedText, "You clicked: Cancel");
    }

    @Test
    public void alertTest3(){

        driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();

        Alert alert = driver.switchTo().alert();

        alert.sendKeys("Hello!");

        alert.accept();

        String expectedText = driver.findElement(By.id("result")).getText();

        Assert.assertEquals(expectedText, "You entered: Hello!");
    }

    @Test
    public void alertTest4(){

        driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();

        Alert alert = driver.switchTo().alert();

        alert.accept();

        String expectedText = driver.findElement(By.id("result")).getText();

        Assert.assertEquals(expectedText, "You entered:");

    }

    @Test
    public void alertTest5() {

        driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();

        Alert alert = driver.switchTo().alert();

        alert.sendKeys("Bye!");

        alert.dismiss();

        String expectedText = driver.findElement(By.id("result")).getText();

        Assert.assertEquals(expectedText, "You entered: null");

    }

    @Test
    public void alertTest6(){

        driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();

        Alert alert = driver.switchTo().alert();

        alert.dismiss();

        String expectedText = driver.findElement(By.id("result")).getText();

        Assert.assertEquals(expectedText, "You entered: null");
    }
}
