package org.semeniv.downloadfiles;

import org.apache.commons.io.FileUtils;

import java.io.*;

public class MyFileUtils {

    public static void createDownloadDirectory() {
        createDirectory(DirectoryFor.DOWNLOAD);
    }

    public static void createDirectory(DirectoryFor directoryFor) {
        File directory = new File(directoryFor.getDirName());
        if (directory.exists()) {
            try {
                FileUtils.cleanDirectory(directory);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            directory.mkdir();
        }
    }

    public enum DirectoryFor {
        DOWNLOAD("download");

        private String dirName;

        DirectoryFor(String dirName) {
            this.dirName = dirName;
        }

        public String getDirName() {
            return dirName;
        }
    }

    public static String readFileInString(String dataFile) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(dataFile));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }
}